#!/usr/bin/python

import hashlib
from itertools import *

s1 = "mary had a little lamb whose fleece was white as snow it followed her to school one day which was against the rules. it made the children laugh"
s2 = "mary had a little LAMB whose fleece was white as snow it followed her to SCHOOL one day which was against the rules. IT made the children laugh"
s3 = "twinkle twinkle little star how I wonder what you are"

ngram_size = 2

# returns a list of ngrams (where n=size) based on the list of tokens in ls
def ngrm(size,ls):
    for s in range(0, len(ls)):
        yield list(islice(ls,s,s+size))

#helper method - just removes trailing ngrams that don't match the required size
def ngram(ls):
    ng = ngrm(ngram_size,ls)
    return ifilter(lambda x: len(x) == ngram_size, ng)

def hash(str):
    return int(hashlib.md5(str).hexdigest(), 16)

# returns the first 2 ngram hash values (sorted numerically)
def minhashed(str,matches):
    tokens = str.split(' ')
    ngrams = ngram(tokens)
    mhashed = sorted(map(lambda i: hash( "".join(i)), ngrams))
    return (list(mhashed))[:matches]

print minhashed(s1,2)
print minhashed(s2,2)
print minhashed(s3,2)
