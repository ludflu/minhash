# minhash
my toy implementation of minhash - written mainly for my own pleasure and edification

the MinHash algorithm can do probabalistic clustering in linear time by aproximating the [Jaccard similarity coefficient](http://en.wikipedia.org/wiki/Jaccard_index)

Much inspiration taken from [Jeff Larson's talk on minhashing](https://github.com/papers-we-love/papers-we-love/issues/197) based on [Andrei Z. Broder's paper.](http://gatekeeper.dec.com/ftp/pub/dec/SRC/publications/broder/positano-final-wpnums.pdf)
